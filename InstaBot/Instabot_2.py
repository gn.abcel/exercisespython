## Instabot 
## Check who are unfollow you.

from time import sleep
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from secrets import pw
driver = webdriver.Chrome(ChromeDriverManager().install())

class InstaBot:
    def __init__(self, username, pw):
        self.driver = webdriver.Chrome()
        self.driver.get("https://instagram.com")
        sleep(2)
        #c"/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div[2]/div/label/input").click()
        self.driver.find_element_by_xpath("//input[@name=\"username\"]") \
            .send_keys(username)
        sleep(3)
        self.driver.find_element_by_xpath("//input[@name=\"password\"]") \
            .send_keys(pw)
        sleep(3)
        #self.driver.find_element_by_xpath("//button[@type=\"submit\"]") \
        self.driver.find_element_by_xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div[1]/div[3]/button") \
            .click()
        sleep(4)
        self.driver.find_element_by_xpath("//button[contains(text(), 'Not Now')]") \
            .click()
        sleep(4)
        self.driver.find_element_by_xpath("//button[contains(text(), 'Not Now')]") \
            .click()
        sleep(2)

    def get_unfollowers(self):
        self.driver.find_element_by_xpath("/html/body/div[1]/section/main/section/div[3]/div[1]/div/div/div[2]/div[1]/div/div/a") \
            .click()
        sleep(2)
        self.driver.find_element_by_xpath("//a[contains(@href,'/following')]") \
            .click()
        following = self._get_names()
        sleep(10)
        self.driver.find_element_by_xpath("//a[contains(@href,'/followers')]") \
            .click()
        followers = self._get_names()
        not_following_back = [user for user in following if user not in followers]
        ignorados = [user for user in followers if user not in following]
        print('Los que no te dieron el follow back:', not_following_back)
        print('No followbackeados :', ignorados)

    def _get_names(self):
        #sleep(2)
        #sugs = self.driver.find_element_by_xpath('//h4[contains(text(), Suggestions)]')
        #self.driver.execute_script('arguments[0].scrollIntoView()', sugs)
        sleep(2)
        #scroll_box = self.driver.find_element_by_xpath("/html/body/div[4]/div/div/div[2]")
        last_ht, ht = 0, 1
        while last_ht != ht:
            last_ht = ht
            sleep(2)
            ht = self.driver.execute_script("""
                arguments[0].scrollTo(0, arguments[0].scrollHeight); 
                return arguments[0].scrollHeight;
                """, self.driver.find_element_by_xpath("/html/body/div[5]/div/div/div[2]"))
        links = self.driver.find_element_by_xpath("/html/body/div[5]/div/div/div[2]").find_elements_by_tag_name('a')
        names = [name.text for name in links if name.text != '']
        # close button
        self.driver.find_element_by_xpath("/html/body/div[5]/div/div/div[1]/div/div[2]/button") \
            .click()
        return names

my_bot = InstaBot('guido.7z', pw)
my_bot.get_unfollowers()

