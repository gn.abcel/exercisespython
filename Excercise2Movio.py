# Excersive 2
# Guido Aspauzo 11/08/2020

from random import sample
import re

def randomPINs():
    i = 0
    pins = []
    while i < 1000:
        pin = ''.join(str(e) for e in sample(range(10), 4))
        regex1 = r'(012|123|234|345|456|567|678|789)'  # Regex for two consecutive digits should not be the same
        regex2 = r'(\d)\1'         # Regex for three consecutive digits should not be incremental
        if not re.search(regex1, pin) and not re.search(regex2, pin):
            i += 1
            pins += [pin]
    return pins

def validations_test(pins):
    for line in pins:
        regex1 = r'(012|123|234|345|456|567|678|789)'    # Regex for two consecutive digits should not be the same
        regex2 = r'(\d)\1'          # Regex for three consecutive digits should not be incremental
        if len(line) != 4 or re.search(regex1, line) is not None or re.search(regex2, line) is not None:
            print('Pin not valid: ' + line)
        else:
            print('Pin valid : ' + line)

validations_test(randomPINs())

# Negative Validations
validations_test(['1103'])  # Validation for two consecutive digits
validations_test(['1230'])  # Validation fot three consecutive digits should not be incremental



