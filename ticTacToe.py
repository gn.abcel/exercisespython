#! python3
# ticTacToe

theBoard = {'top-L': ' ', 'top-M': ' ', 'top-R': ' ',
            'mid-L': ' ', 'mid-M': ' ', 'mid-R': ' ',
            'low-L': ' ', 'low-M': ' ', 'low-R': ' '}

def printBoard(board):
    print(board['top-L'] + '|' + board['top-M'] + '|' + board['top-R'])
    print('-+-+-')
    print(board['mid-L'] + '|' + board['mid-M'] + '|' + board['mid-R'])
    print('-+-+-')
    print(board['low-L'] + '|' + board['low-M'] + '|' + board['low-R'])


def endGame():
    winConditions = [['top-L', 'top-M', 'top-R'], ['mid-L', 'mid-M', 'mid-R'], ['low-L', 'low-M', 'low-R'], ['top-L', 'mid-L', 'low-L'], ['top-M', 'mid-M', 'low-M'], ['top-R', 'mid-R', 'low-R'], ['low-L', 'mid-M', 'top-R'], ['top-L', 'mid-M', 'low-R']]
    for i in range(7):
        if theBoard[winConditions[i][0]] == theBoard[winConditions[i][1]] == theBoard[winConditions[i][2]] and theBoard[winConditions[i][0]] != ' ':
            return True
    return False

turn = 'X'
printBoard(theBoard)
for i in range(9):
    print('Turn for ' + turn + '. Move on which space?')
    move = input()
    print(move)
    if move in theBoard and theBoard[move] == ' ':
        theBoard[move] = turn
        if endGame():
            printBoard(theBoard)
            print('END GAME. The winner is: ' + turn)
            exit()
    else:
        print('Illegal move. Please, type only: top-L, top-M, top-R, mid-L, mid-M, mid-R, low-L, low-M, low-R')
    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'
printBoard(theBoard)

print('Its a tie')